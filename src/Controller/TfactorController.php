<?php

namespace Drupal\tfactor\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;


/**
 * Class TfactorController.
 */
class TfactorController extends ControllerBase {

  protected $connection;
  protected $EXPIRATION_TIME = 900;

  public function __construct() {
    $this->connection = \Drupal::database();
  }

  /**
   * Sends the generated code, via mail
   *
   * @param $account
   * @param int $length
   *
   * @return int The account id of the user that is requesting the code
   * @throws \Exception
   */
  public function sendCode($account, $length = 6) {

    $token = $this->generateCode($length);

    $this->connection->delete('tfa_token')
      ->condition('user_id', $account->id())
      ->execute();

    $this->connection->insert('tfa_token')
      ->fields([
        'token' => $token,
        'user_id' => $account->id(),
        'created' => \Drupal::time()->getRequestTime(),
        'changed' => \Drupal::time()->getRequestTime(),
      ])
      ->execute();

    $sent = $this->sendMail($account->getEmail(),$token);
    $messenger = \Drupal::messenger();

    if ($sent) {
      $messenger->addMessage('An authentication token has been sent to your connected email address. Please enter it here.');
      return TRUE;
    }
    else {
      return FALSE;
    }
  }


  /**
   * Gets the token and checks if the token is valid before finalizing the account login
   * After logging in the tfa token gets updated to reflect the used state and unsets the tempstorage variables
   *
   * @param $token
   *
   * @return bool
   */
  public function authenticateToken($token)
  {
    $tempStore = \Drupal::service('user.private_tempstore')->get('tfactor');
    $accountId = $tempStore->get('accountId');

    if(isset($accountId) && $accountId > 0) {
      $query = $this->connection->select('tfa_token', 't')
        ->fields('t', ['token', 'user_id', 'created','used'])
        ->condition('user_id', $accountId);
      $storedToken =  $query->execute()->fetch();

      if ($this->tokenValid($storedToken) && $this->userSubmittedTokenMatch($storedToken, $token)) {
        $account = User::load($accountId);
        user_login_finalize($account);

        //unset tempstorage
        $tempStore->delete('accountId');

        //set token used to one
        $this->connection->update('tfa_token')
          ->fields(['used' => 1])
          ->condition('user_id', $accountId)
          ->execute();

        return true;
      }
    }
    return false;
  }

  /**
   * Check to see if the token is still valid
   * It is valid if A: the time is not exceeded and B: if the token has NOT been used yet.
   * The expiration time is set to 900 seconds (15 minutes) by default and can be changed in the properties of this class.
   *
   * @param $token
   *
   * @return bool
   */
  public function tokenValid($token)
  {
    $time = \Drupal::time()->getRequestTime();
    $expiresAt = (int)$token->created + $this->EXPIRATION_TIME;
    $expired = (int)$time > (int)$expiresAt;

      if (!$expired && $token->used == 0){
       return TRUE;
      }
      return FALSE;
  }

  /**
   * Checks if the submitted token matches the token in the database
   *
   * @param $storedToken
   * @param $token
   *
   * @return bool
   */
  public function userSubmittedTokenMatch($storedToken, $token)
  {
      if ($token === $storedToken->token){
        return true;
      }
      return false;
  }

  /**
   * Generates the mail to be sent to the given email address.
   *
   * @param $email
   * @param $code
   *
   * @return bool
   */
  public function sendMail($email, $code)
  {
    $mailManager = \Drupal::service('plugin.manager.mail');

    $to = $email;
    $params['message'] = 'Your authorisation code is: '.$code;
    $params['title'] = '[2FA] - Authorisation';
    $key = 'mail_code';
    $module = 'tfactor';

    $handled = $mailManager->mail($module, $key, $to, \Drupal::languageManager()->getDefaultLanguage(), $params, NULL, TRUE);
    if ($handled['result']) {
      return TRUE;
    }
    return FALSE;
  }


  /**
   * Generates a pseudo-random code specified by length.
   *
   * @param $length
   *
   * @return string: the specified code
   */
  public function generateCode($length) {
    $randomBytes = openssl_random_pseudo_bytes($length);
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $result = '';
    for ($i = 0; $i < $length; $i++) {
      $result .= $characters[ord($randomBytes[$i]) % $charactersLength];
    }
    return $result;
}

  /**
   * Checks if the accountId is set and matches a user.
   *
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   */
  public function access() {
    $tempStore = \Drupal::service('user.private_tempstore')->get('tfactor');
    $accountId = $tempStore->get('accountId');

    if (!isset($accountId) or $accountId === 0){
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

}
