<?php

/**
 * @file
 * Contains Drupal\tfactor\Form\TfactorForm.
 */

namespace Drupal\tfactor\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tfactor\Controller\TfactorController;

/**
 * Class ImportForm.
 *
 * @package Drupal\tfactor\Form
 */
class TfactorForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tfactor_auth';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Authentication token'),
      '#attributes' => ['placeholder' => 'Place your token here']
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $token = $form_state->getValue('token');
    $controller = new TfactorController();
    if($controller->authenticateToken($token)){
      $form_state->setRedirect('user.page');
    }
    else {
      $form_state->setErrorByName('token', 'Token invalid');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }


}
